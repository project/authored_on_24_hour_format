/**
 * @file
 * Set "flatpickr" - javascript datetime picker for the "Authored on" field.
 */

(function () {
  
  'use strict';

  flatpickr('#edit-created-0-value-time', {
    enableTime: true,
    noCalendar: true,
    dateFormat: "H:i",
    time_24hr: true
  });
  
  // Change date format.
  /*
  flatpickr('#edit-created-0-value-date', {
    enableTime: false,
    altInput: true,
    altFormat: "j.m.Y",
    dateFormat: "Y-m-d",    
  });
  */

  // Change the "format" in the description under the date field.
  // var description = document.getElementById('edit-created-0-value--description');
  // description.children[0].textContent = 'My new format';

})();
